let xhr = new XMLHttpRequest();

xhr.open('GET', 'json/posts.json', false);
xhr.send();
let units = JSON.parse(xhr.responseText);

localStorage.setItem("units", JSON.stringify(units) );
document.getElementById("post-add").addEventListener("submit", addNewPost);

let posts = document.getElementById("posts");
  for (let i = 0; i < units.length; i++) {
    let article = document.createElement("article");
    article.id = units[i].id;
    let header = document.createElement("header");
    let h3 = document.createElement("h3");
    h3.textContent = units[i].title;
    header.appendChild(h3);

    let section = document.createElement("section");
    let p = document.createElement("p3");
    p.textContent = units[i].body;
    section.appendChild(p);

    let footer = document.createElement("footer");
    let div = document.createElement("div");
    div.classList.add("tags");
    let firstTagBtn = document.createElement("button");
    firstTagBtn.classList.add("btn", "btn-xs", "btn-default");
    firstTagBtn.textContent = units[i].tags[0];

    let secondTagBtn = document.createElement("button");
    secondTagBtn.classList.add("btn", "btn-xs", "btn-default");
    secondTagBtn.textContent = units[i].tags[1];
    div.appendChild(firstTagBtn);
    div.appendChild(secondTagBtn);
    footer.appendChild(div);

    article.appendChild(header);
    article.appendChild(section);
    article.appendChild(footer);
    posts.appendChild(article);
  }

  let listOfPosts = document.querySelectorAll("article");
  for (let i = 0; i < listOfPosts.length; i++) {
    let deletePost = document.createElement("div");
    deletePost.className = "controls";
    let deletePostBtn = document.createElement("button");
    deletePostBtn.classList.add("btn", "btn-danger", "btn-mini");
    deletePostBtn.textContent = "Delete";
    deletePost.appendChild(deletePostBtn);
    listOfPosts[i].appendChild(deletePost);
    listOfPosts[i].addEventListener("click", onDeletePost);
  }

  function onDeletePost(event) {
    let elementToRemove = event.target;
    if (elementToRemove.tagName === "BUTTON") {
      for (let i = 0; i < units.length; i++) {
        if (units[i].id === +elementToRemove.parentElement.parentElement.id) {
          units.splice(i, 1);
        }
      }

      localStorage.setItem("units", JSON.stringify(units) );

      elementToRemove.parentElement.parentElement.style.display = "none";
    }

  }

  let headerVal = document.getElementById("header-value");
  let body = document.getElementById("body");
  function addNewPost(event) {
    event.preventDefault();
    let strToSplit = document.getElementById("tags").value;
    let res = strToSplit.split(",");
    units.push({id: units[units.length -1].id + 1, title: headerVal.value, body: body.value, tags: [res], });
    localStorage.setItem("units", JSON.stringify(units) );

    let article = document.createElement("article");
    article.id = units[units.length -1].id;
    let header = document.createElement("header");
    let h3 = document.createElement("h3");
    h3.textContent = headerVal.value;
    header.appendChild(h3);

    let section = document.createElement("section");
    let p = document.createElement("p3");
    p.textContent = body.value;
    section.appendChild(p);

    let footer = document.createElement("footer");
    let div = document.createElement("div");
    div.classList.add("tags");
    let firstTagBtn = document.createElement("button");
    firstTagBtn.classList.add("btn", "btn-xs", "btn-default");
    firstTagBtn.textContent = res[0];

    let secondTagBtn = document.createElement("button");
    secondTagBtn.classList.add("btn", "btn-xs", "btn-default");
    secondTagBtn.textContent = res[1];
    div.appendChild(firstTagBtn);
    div.appendChild(secondTagBtn);
    footer.appendChild(div);

    article.appendChild(header);
    article.appendChild(section);
    article.appendChild(footer);

    let deletePost = document.createElement("div");
    deletePost.className = "controls";
    let deletePostBtn = document.createElement("button");
    deletePostBtn.classList.add("btn", "btn-danger", "btn-mini");
    deletePostBtn.textContent = "Delete";
    deletePost.appendChild(deletePostBtn);
    article.appendChild(deletePost);
    posts.appendChild(article);
    deletePost.addEventListener("click", onDeletePost);
    document.getElementById("post-add").reset();
  }